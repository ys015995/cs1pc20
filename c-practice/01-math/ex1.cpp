#include <stdio.h>
#include <assert.h>

// Function to be completed
int calculateSum(int a, int b) {
    // Hint: Choose the correct operator to calculate the sum
    // Options: '+', '-', '*', '/'
    // Your code here
    int ans = a + b;
    // Hint: Don't forget to return the calculated sum
    // Your code here
    return ans;
}

// Function to perform tests
void runTests() {
    int result1 = calculateSum(5, 3);
    assert(result1 == 8);

    int result2 = calculateSum(-2, 8);
    assert(result2 == 6);

    // Add more test cases if needed
    int result3 = calculateSum(500,-80);
    assert(result3 == 420);
}

int main() {
    // Call the test function
    runTests();

    printf("All tests passed successfully!\n");

    return 0;
}
