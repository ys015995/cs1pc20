#include <iostream>
using namespace std;

class floatList {
    private:
        struct ListNode {
            float value;
            struct ListNode* next;
        };

        ListNode* head;
    
    public:
        floatList(void) {
            head = nullptr;
        };
        ~floatList(void) { };
        
        void appendNode(float n) {

            ListNode *temp = new ListNode;
            temp->value = n;
            temp->next = NULL;

            if (head == NULL) {
                head = temp;
            } else {
                ListNode *count = head;
                while (count->next != NULL) {
                    count = count->next;
                }
                count->next = temp;
            }

        };

        void displayList(void) {
            ListNode *count = head;
            
            if (head == NULL) {
                cout << "Empty List" << endl;
            }

            while (count != NULL) {
                cout << count->value << endl;
                count = count->next;
            }
        };

        void deleteNode(float rmv) {
            ListNode *count = head;

            if (head->value == rmv) {
                head = count->next;
                cout << rmv << " is deleted" << endl;
                return;
            }

            while (count->next->value != rmv) {
                count = count->next;
            }
            count->next = count->next->next;
            cout << rmv << " is deleted" << endl;
        };
};
