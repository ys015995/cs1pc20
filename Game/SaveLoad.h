#include <iostream>
#include <map>
#include <fstream>
#include <sstream>
#include <vector>
#include "Player.h"
#include "Area.h"

class SaveLoad {
    private:
        Area map;
        Player player;
    public:

        SaveLoad() : player("placeholder", 0) {
            map = Area();
        }

        Player* getPlayer() {
            return &player;
        }

        void LoadMap() {
            map.loadMapFile("rooms.txt");
        }

        void setPlayer(Player c) {
            player = c;
        }

        void LoadPlayer() {
            std::ifstream inputFile("player.txt");

            if (!inputFile.is_open()) {
                return;
            }

            std::string name; std::getline(inputFile,name);
            std::string healthS; std::getline(inputFile,healthS);
            int healthInt = std::stoi(healthS); 
            setPlayer(Player(name, healthInt));

            std::string location; std::getline(inputFile, location);
            player.setLocation(map.getRoom(location));

            std::string itemLine;
            while (std::getline(inputFile, itemLine)) {
                int sep1 = itemLine.find("|");
                int sep2 = itemLine.find("|",sep1+1);
                std::string name = itemLine.substr(0,sep1);
                std::string desc = itemLine.substr(sep1+1,sep2-sep1-1);
                bool pick = (std::stoi(itemLine.substr(sep2+1)) == 1);
                Item newItem(name, desc, pick);
                player.receiveItem(newItem);
            }

            inputFile.close();
        }

        void LoadAll() {
            LoadMap();
            LoadPlayer();
        }

        void SaveMap() {
            std::ofstream output("rooms.txt");

            if (!output.is_open()) {
                return;
            }

            for (const auto &keyValue : map.getMap()) {
                output << "Room|";
                output << keyValue.first << "|";
                output << keyValue.second->getDescription() << std::endl;
            }

            for (const auto &keyValue : map.getMap()) {
                for (const auto &connections : keyValue.second->getAllExits()) {
                    output << "Connection|";
                    output << keyValue.first << "|";
                    output << connections.second->getName() << "|";
                    output << connections.first << std::endl;
                }
                Inventory tempInv = keyValue.second->getRoomInv();
                for (auto & element : tempInv.getList()) {
                    output << "Item|";
                    output << keyValue.second->getName() << "|";
                    output << element.getName() << "|";
                    output << element.getDescription() << "|";
                    output << element.getStatusInt() << std::endl;
                }
                if (keyValue.second->hasTrap()) {
                    Trap tempTrap = keyValue.second->getTrap();
                    output << "Trap|";
                    output << keyValue.second->getName() << "|";
                    output << tempTrap.getName() << "|";
                    output << std::to_string(tempTrap.dealDamage()) << "|";
                    output << std::to_string(tempTrap.trapHits()) << "|";
                    if (tempTrap.getOccurance()) {
                        output << "1" << std::endl;
                    } else {
                        output << "0" << std::endl;
                    }
                }
            }

            output.close();

        }

        void SavePlayer() {
            std::ofstream output("player.txt");

            if (!output.is_open()) {
                return;
            }

            output.clear();
            output << player.getName() << std::endl;
            output << player.getHealth() << std::endl;
            output << player.getLocation()->getName() << std::endl;

            Inventory temp = player.getInventory();

            for (int i = 0; i < temp.getSize(); i++) {
                Item current = temp.getItemObjInt(i);
                output << current.getName();
                output << "|" << current.getDescription() << "|";
                output << current.getStatusInt() << std::endl;
            }

            output.close();
        }

        void SaveAll() {
            SaveMap();
            SavePlayer();
        }

};