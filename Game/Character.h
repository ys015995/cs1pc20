#include <iostream>
#include <string>
#include <vector>
#include "Inventory.h"
#pragma once

class Character {
    private:
        std::string name;
        int health;
        Inventory inv;
        bool status;

    public:

        Character(const std::string& n, int hp) {
            name = n;
            health = hp;
            status = true;
        }

        std::string getName() {
            return name;        //returns the character's name
        }

        int getHealth() {
            return health;      //returns the character's current health
        }

        bool getStatus() {
            return status;      //returns the character's current status
        }   

        Inventory getInventory() {
            return inv;     //returns the character's inventory
        }

        void takeDamage(int damage) {
            health -= damage;       //changes character's health based on damage
            if (health <= 0) {      //if the character's health reaches below 0, status changes
                status = false;
            }
        }

        void receiveItem(const Item& item) {
            inv.addItem(item);      //adds an item to the character's inventory
        }

        void removeItem(const Item& item) {
            inv.deleteItem(item);       //deletes an item from the character's inventory
        }

        std::string lowerCase(std::string original) {
            //returns the original string in complete lowercase
            std::string n = "";
            for (int i = 0; i < original.length(); i++) { 
                n += (char) tolower(original.at(i));
            }
            return n;
        }
};
