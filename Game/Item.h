#include <iostream>
#include <string>
#pragma once

class Item {
    private:
        std::string name;
        std::string description;
        bool usable;

    public:
        Item(const std::string& n, const std::string& desc, bool status) {
            name = n;
            description = desc;
            usable = status;
        }

        std::string getName() const {
            return name;        //returns the item's name
        }

        std::string getDescription() {
            return description;     //returns the item's description
        }

        int getStatusInt() {
            if (usable) { return 1; }       //returns the item's status as a int
            return 0;
        }

        bool getStatusBool() {
            return usable;      //returns the item's status as a boolean
        }

        int interact() {
            return (-20) + rand()%61;       //interact function for potion (will update if more interactable items are added)
        }
};
