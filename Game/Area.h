#ifndef AREA_H
#define AREA_H

#include <iostream>
#include <map>
#include <fstream>
#include <sstream>
#include <vector>
#include "Room.h"

class Area {

    private:
        std::map<std::string, Room*> rooms;
    
    public:

        Area(void) {};

        std::map<std::string, Room*> getMap() {
            return rooms;       //returns the Room Map
        }

        void AddRoom(const std::string& name, Room* room) {
            if (room != nullptr) {      //ensure that the room is not a Nullptr
                rooms.insert({lowerCase(name),room});       //adds a room to the map
            }
        }

        Room* getRoom(const std::string& name) {
            std::string target = lowerCase(name);       //sets the room target
            for (auto it = rooms.begin(); it != rooms.end(); ++it) {
                if (it->first == target) {      //search for the target in the map
                    return it->second;      //returns the room associated to the taarget
                }
            }
            return nullptr;     //if there is no room matching that name
        }

        void connectRooms(const std::string& room1N, const std::string& room2N, const std::string& cardinal) {
            
            //sets everything to lowercase to search
            std::string target = lowerCase(cardinal);
            std::string room1NA = lowerCase(room1N);
            std::string room2NA = lowerCase(room2N);

            /*  Each if statement has three sections:
                The condition that is based on the cardinal direction inputted
                One line to add an exit to another room in the cardinal direction
                Another line to add an exit in the second room to the first one in the opposite direction
            */

                if (target == "north") {
                    getRoom(room1NA)->AddExit(target, getRoom(room2NA));     
                    getRoom(room2NA)->AddExit(oppositeD(target), getRoom(room1NA));
                } else if (target == "south") {
                    getRoom(room1NA)->AddExit(target, getRoom(room2NA));
                    getRoom(room2NA)->AddExit(oppositeD(target), getRoom(room1NA));
                } else if (target == "east") {
                    getRoom(room1NA)->AddExit(target, getRoom(room2NA));
                    getRoom(room2NA)->AddExit(oppositeD(target), getRoom(room1NA)); 
                } else if (target == "west") {
                    getRoom(room1NA)->AddExit(target, getRoom(room2NA));
                    getRoom(room2NA)->AddExit(oppositeD(target), getRoom(room1NA));
                } else {
                    std::cout << "Invalid input. No connection made." << std::endl;     //target is not valid
                }
        }

        std::string oppositeD(std::string direction) {
            //returns the opposite cardinal direction
            if (direction == "north") return "south";
            if (direction == "south") return "north";
            if (direction == "east") return "west";
            if (direction == "west") return "east";
            return "";
        }

        int findNext(std::string line, int min) {
            //finds the next instance of the seperator "|" in the inputted string
            return line.find("|", min+1);
        }

        void nextSubstr(std::string line, int &pos1, int &pos2) {
            //updates the position counters to prepare for finding the next substring
            pos1 = pos2;
            pos2 = findNext(line, pos1);
        }

        std::string getSubstr(std::string line, int pos1, int pos2, bool end) {
            if (!end) {     //if the substring is not the one at the end of the string
                return line.substr(pos1+1, pos2-pos1-1);        //returns a substring between pos1 and pos2
            } else {        //if the substring is at the end of the string
                return line.substr(pos2+1);     //returns what is left
            }
        }

        bool intBool(int iB) {
            return (iB == 1);
        }

        void loadMapFile(const std::string& file) {
            std::string fileName = file;
            std::ifstream inputFile(fileName);

            if (!inputFile.is_open()) {
                //error opening file
                return;
            }
            
            std::string currentLine;

            while (std::getline(inputFile,currentLine)) {       //loop through all lines in the file
                
                if (!currentLine.empty()) {     //ensure that the line is not empty

                    int findPos1 = currentLine.find("|");
                    int findPos2 = findNext(currentLine, findPos1);
                    std::string select = getSubstr(currentLine,-1,findPos1,false);      //search for the first word in the line

                    if (select == "Room") {     //add a room
                        std::string name = getSubstr(currentLine,findPos1,findPos2,false);      //get room name
                        std::string desc = getSubstr(currentLine,findPos1,findPos2,true);       //get room description
                        Room* newRoom = new Room(name,desc);        //create the room
                        this->AddRoom(name, newRoom);       //add room to the map
                    } else if (select == "Item") {      //add a item
                        std::string room = getSubstr(currentLine,findPos1,findPos2,false);      //get item location
                        nextSubstr(currentLine, findPos1, findPos2);
                        std::string name = getSubstr(currentLine,findPos1,findPos2,false);      //get item name
                        nextSubstr(currentLine, findPos1, findPos2);
                        std::string desc = getSubstr(currentLine,findPos1,findPos2,false);      //get item location
                        bool pick = intBool(std::stoi(getSubstr(currentLine,findPos1,findPos2,true)));      //get usability
                        Item* newItem = new Item(name, desc, pick);     //create the item
                        this->getRoom(room)->AddItem(*newItem);     //add the item to the correct room
                    } else if (select == "Connection") {        //add a room exit/connection
                        std::string room1 = getSubstr(currentLine,findPos1,findPos2,false);     //get first room
                        nextSubstr(currentLine, findPos1, findPos2);
                        std::string room2 = getSubstr(currentLine,findPos1,findPos2,false);     //get second room
                        std::string cardinal = getSubstr(currentLine,findPos1,findPos2,true);       //get cardinal direction
                        connectRooms(room1,room2,cardinal);     //connect the two rooms
                    } else if (select == "Trap") {      //add a trap
                        std::string location = getSubstr(currentLine,findPos1,findPos2,false);      //get trap location
                        nextSubstr(currentLine, findPos1, findPos2);
                        std::string name = getSubstr(currentLine,findPos1,findPos2,false);      //get trap name
                        nextSubstr(currentLine, findPos1, findPos2);
                        int damage = std::stoi(getSubstr(currentLine,findPos1,findPos2,false));     //get trap damage
                        nextSubstr(currentLine, findPos1, findPos2);
                        int chance = std::stoi(getSubstr(currentLine,findPos1,findPos2,false));     //get trap occurance rate
                        bool once = intBool(std::stoi(getSubstr(currentLine,findPos1,findPos2,true)));      //get reoccurance status
                        Trap temp(name,damage,chance,once);     //create the trap
                        this->getRoom(location)->addTrap(temp);     //add the trap to the location
                    } else {        //Not valid
                        //error
                    }

                }

            }
            inputFile.close();
        }

        std::string lowerCase(std::string original) {       //returns the original string but in complete lowercase
            std::string n = "";
            for (int i = 0; i < original.length(); i++) { 
                n += (char) tolower(original.at(i));
            }
            return n;
        }

};

#endif