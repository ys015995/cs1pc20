#include <iostream>
#include <vector>
#include <string>
#include "Character.h"
#include "Room.h"
#pragma once

class Player: public Character {
    private:
        Room* location;
    
    public:
        Player(std::string pName, int pHealth) : Character(pName, pHealth), location(nullptr) {};       //creates player

        void setLocation(Room* set) {
            location = set;     //sets player's current location
        }

        Room* getLocation() {
            return location;        //returns the player's current location
        }

        

};