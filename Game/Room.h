#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include "Trap.h"
#include "Inventory.h"
#pragma once

class Room {
    private:
        std::string name;
        std::string description;
        std::map<std::string,Room*> exits;
        Inventory itemsAround;
        std::vector<Trap> trap;
    
    public:
        
        Room(const std::string n,const std::string desc) {
            name = n;
            description = desc;
        }

        std::string getName() {
            return name;        //returns the Room's name
        }

        std::string getDescription() {
            return description;     //return the Room's description
        }

        Inventory getRoomInv() {
            return itemsAround;     //returns the Room's inventory
        }

        std::map<std::string,Room*> getAllExits() {
            return exits;       //returns the Room's exits map
        }

        std::vector<Trap> getAllTraps() {
            return trap;        //returns the Room's traps list
        }

        bool hasItem() {
            return itemsAround.hasItem();       //returns wehther or not the inventory list is empty
        }

        Item getItem() {
            return itemsAround.getList()[0];        //returns the first item in the inventory list
        }
        
        void AddItem(Item& item) {
            itemsAround.addItem(item);      //adds a item to the inventory list
        }

        void RemoveItem() {
            itemsAround.deleteFirst();      //removes the first item in the inventory list
        }

        void AddExit(const std::string cardinal, Room* loc) {
            std::string direct = lowerCase(cardinal);
            exits.insert({direct, loc});        //creates a new exit to another room via a cardinal direction
        }

        Room* getExit(std::string cardinal) {
            std::string direct = lowerCase(cardinal);
            for (auto it = exits.begin(); it != exits.end(); ++it) {            //searches for the input
                if (it->first == direct) {      //checks if the direction is the current one in the map
                    return it->second;      //returns the room the exit connects to
                }
            }
            return nullptr;
        }

        bool hasTrap() {
            return !trap.empty();       //returns if the room has a trap or not
        }

        void addTrap(Trap newTrap) {
            trap.push_back(newTrap);        //adds a trap to the room
        }

        void removeTrap() {
            trap.erase(trap.begin());       //removes a trap from the room
        }

        Trap getTrap() {
            return trap[0];     //gets the first trap in the vector
        }

        std::string lowerCase(std::string original) {
            //returns the original string in complete lowercase
            std::string n = "";
            for (auto& x : original) { 
                n += (char) tolower(x);
            }
            return n;
        }

};