#include <iostream>
#include <string>

class Trap {
    private:
        std::string trapName;
        int damageDealt;
        int probOfActivate;
        bool oneTime;
    public:

        Trap() {};

        Trap(std::string tn, int dD, int p, bool time) {
            trapName = tn;
            damageDealt = dD;
            probOfActivate = p;
            oneTime = time;
        }

        bool trapHits() {
            int occur = rand()%100;
            if (occur <= probOfActivate) {
                return true;
            } else {
                return false;
            }
        }

        bool getOccurance() {
            return oneTime;
        }

        std::string getName() {
            return trapName;
        }

        int dealDamage() {
            return damageDealt;
        }

        
};