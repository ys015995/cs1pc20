#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "Item.h"
#pragma once

class Inventory {
    private:
        std::vector<Item> items;

    public:

        Inventory() {}

        std::vector<Item> getList() {
            return items;       //returns the entire list in full 
        }

        int getSize() {
            return items.size();        //returns the current size of the inventory list
        }

        void addItem(const Item& item) {
            items.push_back(item);      //adds an item to the inventory
        }

        void deleteItem(const Item& item) {
            items.erase(std::remove_if(items.begin(), items.end(),
                        [&item](const Item& itemS) {
                            return itemS.getName() == item.getName();       //searches and deletes the inputted item
                        }), items.end());
        }

        void deleteFirst() {
            items.erase(items.begin());     //deletes the first item in the inventory
        }

        bool hasItem() {
            return !items.empty();      //returns whether or not there is an item in the inventory
        }

        Item getItemObjInt(int loc) {
            if (loc > -1 && loc < items.size()) {       //ensures that the numerical input is valid
                return items[loc];      //returns the item from the location
            }
            return items[-1];
        }

        int getItemLocStr(std::string item) {
            int count = 0;
            int loc = 0;
            for (auto it = items.begin(); it != items.end(); ++it) {        //loops through entire inventory
                Item temp = items[loc];
                if (lowerCase(temp.getName()) == lowerCase(item)) {     //checks if the current item is the same as the inputted one
                    return count;       //returns counter/int location of the item
                }
                count++; loc++;         //increase counter
            }
            return -1;
        }

        Item getItemObjStr(std::string name) {
            int loc = 0;
            for (auto it = items.begin(); it != items.end(); ++it) {        //loops through entire inventory
                if (lowerCase(items[loc].getName()) == lowerCase(name)) {       //checks if the current item is the same as the input
                    return items[loc];      //returns the item
                }
                loc++;      //increase counter
            }
            return items[-1];
        }

        std::string lowerCase(std::string original) {
            //returns the original string in complete lowercase
            std::string n = "";
            for (int i = 0; i < original.length(); i++) { 
                n += (char) tolower(original.at(i));
            }
            return n;
        }
};