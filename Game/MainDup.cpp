#include <iostream>
#include <vector>
#include <map>
#include "SaveLoad.h"
#include "CommandInterpreter.h"

int main() {

    //area
    SaveLoad file;

    //load from file
    file.LoadAll();

    //creating player and setting player status
    Player* p1 = file.getPlayer();

    CommandInterpreter interpreter(p1);
    std::string choice = "";

    while (p1->getStatus()) {       //loop while the player is alive
        try {
            //prints Character Information
            std::cout << "" << std::endl;
            std::cout << p1->getName() << std::endl;
            std::cout << "HP: " << p1->getHealth() << std::endl; 
            std::cout << "Current Location: " << p1->getLocation()->getDescription() << std::endl;
            std::cout << "Inventory: " << std::endl;
            for (int i = 0; i < p1->getInventory().getSize(); i++) {
                Item temp = p1->getInventory().getItemObjInt(i);
                std::cout << "- " << temp.getName() << ", " << temp.getDescription() << std::endl;
            }
            std::cout << "" << std::endl;

            //prints user options
            std::cout << "Please input a option below:" << std::endl;
            std::cout << "Move (M) | ";
            std::cout << "Look Around (LA) | ";
            std::cout << "Pick Up Item (PUI) | ";
            std::cout << "Interact with Item (I) | ";
            std::cout << "Drop Item (DI) | ";
            std::cout << "Quit (Q)" << std::endl;
            std::cout << "> ";

            //get user input
            std::cin >> choice;

            //if user decides to quit
            if (choice == "Quit" || choice == "quit" || choice == "q") {
                std::cout << "\n";
                std::cout << "Save in progress... Please wait..." << std::endl;
                //save progress
                file.SaveAll();
                std::cout << "Save complete. Come again soon :) \n" << std::endl;
                break;
            } else {
                //interpret user action
                interpreter.interpretCommand(choice);
            }
            
        } catch (std::exception e) {        //if invalid input in someway
            std::cout << "Something went wrong, resetting" << std::endl;
            std::cout << "" << std::endl;
            std::cout << "" << std::endl;
            choice = "";
        }
    }
    
    if (!p1->getStatus()) {     //if the player dies : Game Over Screen
        std::cout << "GAME OVER\n";
        std::cout << "No save made. Will load last save during next gameplay.\n\n";
    }

    return 0;
}
