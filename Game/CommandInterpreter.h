#include <iostream>
#include <string>
#include <map>
#include "Player.h"

class CommandInterpreter {
    public:
        CommandInterpreter(Player* player) : player_(player) {}
        
        void interpretCommand(const std::string& c) {
            std::string command = lowerCase(c);     //sets the command to lowercase
            if (command == "move" || command == "m") {      //if the command is to move
                std::string cardinal;
                std::cout << "What direction?" << std::endl << "> ";        //asks for the direction the player wants to move
                std::cin >> cardinal;

                Room* nextRoom = player_->getLocation()->getExit(lowerCase(cardinal));      //get the room in that direction
                if (nextRoom != nullptr) {      //ensure the room is exists
                    player_->setLocation(nextRoom);     //changes the player's location
                    std::cout << "You move on to the next room." << std::endl;
                } else {        //if the room does not exist
                    std::cout << "You are unable to proceed in this direction." << std::endl;
                }
            } else if (command == "look around" || command == "la") {       //if the command is to look around the room
                if (player_->getLocation()->hasItem()) {       //checking if the room has a item
                    Item temp = player_->getLocation()->getItem();      //get the item and inform the player
                    std::cout << "You search the room and find a " << temp.getName() << "!" << std::endl;
                } else {        //if there are no items
                    std::cout << "There are no Items left in this room." << std::endl;
                }
                if (player_->getLocation()->hasTrap()) {        //checks if the room has a trap
                        Trap tempTrap = player_->getLocation()->getTrap();
                        if (rand()%100 <= 5) {      //flat 5% chance of the trap activating when found
                            std::cout << "You got hit by a " << tempTrap.getName() << ".\n";
                            player_->takeDamage(tempTrap.dealDamage());
                            if (tempTrap.getOccurance()) {      //delete if trap only occurs once
                                player_->getLocation()->removeTrap();
                            }
                        } else {        //trap fails and informs the player that a trap is here
                            std::cout << "You notice a trap in the room and remain unscathed." << std::endl;
                        }
                    } else {
                        std::cout << "You remain unscathed from your pondering." << std::endl;
                    }
            } else if (command == "pick up item" || command == "pui") {     //if the action is to pick up an item
                if (player_->getLocation()->hasItem()) {       //ensures the room has an item
                    Item temp = player_->getLocation()->getItem();      //gets the item
                    player_->receiveItem(temp);     //adds item to inventory
                    player_->getLocation()->RemoveItem();       //removes item from room 
                    std::cout << temp.getName() << " has been added to your inventory." << std:: endl;
                } else {        //prepares to activate trap if 
                    if (player_->getLocation()->hasTrap()) {        //if the room has a trap
                        Trap tempTrap = player_->getLocation()->getTrap();      //gets the trap
                        if (tempTrap.trapHits()) {      //if the trap hits
                            std::cout << "You got hit by a " << tempTrap.getName() << ".\n";
                            player_->takeDamage(tempTrap.dealDamage());     //take damage from the trap
                            if (tempTrap.getOccurance()) {      //deletes the trap if it only occurs once
                                player_->getLocation()->removeTrap();
                            }
                        } else {        //if there are no items left and the trap fails
                            std::cout << "You remain unscathed as you realise there is nothing to pick up." << std:: endl;
                        }
                    } else {           //if there are no items or traps
                        std::cout << "You remain unscathed as you realise there is nothing to pick up." << std::endl;
                    }
                }
            } else if (command == "drop item" || command == "di") {     //if the action is to drop an item
                std::string item;
                std::cout << "What item?" << std::endl << "> ";     //asks the player for the item to drop
                std::cin >> item;

                if (player_->getInventory().getItemLocStr(item) != -1) {        //ensure that the player has the item
                    Item temp = player_->getInventory().getItemObjStr(item);        //gets the item
                    player_->getLocation()->AddItem(temp);      //adds the item to the room
                    player_->removeItem(temp);      //remove the item from the player inventory
                    std::cout << "You dropped the " << temp.getName() << std::endl;
                } else {        //if item does not exist
                    std::cout << "Item does not exist/is not in your inventory." << std::endl;
                }
            } else if (command == "interact with item" || command == "i") {     //if the action is to interact with an item
                std::string item;
                std::cout << "What item?" << std::endl << "> ";     //asks the user for the item
                std::cin >> item;

                if (player_->getInventory().getItemLocStr(item) != -1) {        //ensure that the item exists
                    Item temp = player_->getInventory().getItemObjStr(item);        //get the item
                    if (temp.getStatusBool()) {     //check if it can be interacted with or not
                        int fromPotion = temp.interact();       //interact with item
                        /*
                        As the only interactable item is currently a potion, the following code is code for the potion.
                        This code would be changed/made more advanced if more interactable items are added to the game. 
                        */
                        player_->takeDamage((-1)*fromPotion);
                        if (fromPotion > 0) {
                            std::cout << "You drank the potion... Slowly, you feel yourself regaining vitality" << std::endl;;
                        } else if (fromPotion < 0) {
                            std::cout << "You drank the potion... Slowly, you feel yourself getting weaker."<< std::endl;;
                        } else {
                            std::cout << "You drank the potion... Yet nothing happened...";
                        }
                        player_->removeItem(temp);
                        std::cout << temp.getName() << " has been removed from your Inventory." << std::endl;
                    } else {        //if item cannot be interacted with
                        std::cout << "You cannot interact with this item" << std::endl;
                    }
                }
            } else {        //if command does not exist
                std::cout << "Unknown command: " << command << std::endl;
            } 
        }

        std::string lowerCase(std::string original) {
            //returns the original string in complete lowercase
            std::string n = "";
            for (int i = 0; i < original.length(); i++) { 
                n += (char) tolower(original.at(i));
            }
            return n;
        }

    private:
        Player* player_; // Pointer to the player object
};
